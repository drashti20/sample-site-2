// Initialise FlexSlider for Carousels
$(window).ready(function() {
    $('.flexslider').flexslider({
    animation: "fade",
    // controlNav: "thumbnails",
    directionNav: true,
    slideshowSpeed: 5000,
    animationSpeed: 600,
    touch: true
    });
});


$(window).ready(function() {
$('#blog-slider.owl-carousel').owlCarousel({
    loop: !0,
    margin: 10,
    nav: !0,
    dots: !1,
    autoplay: !1,
    responsive: {
        0: {
            items: 1
        },
        545: {
            items: 1
        },
        767: {
            items: 2
        },
        991: {
            items: 3
        },
        1000: {
            items: 3
        }
    }
});
});




// function mobileToggleColumn() {
//     if ($(window).width() < 992) {
        
//         $(".footer-strip .footer-mainlink h3").append("<span class='mobile_togglemenu'> </span>");
//         $(".footer-strip .footer-mainlink h3").addClass('toggle');
//         $(".footer-strip .footer-mainlink h3").addClass('active');
//         $(".footer-strip .mobile_togglemenu").click(function() {
//             $(this).parent().toggleClass('active').parent().find('.footer-details').slideToggle('slow')
//         })
//     } else {
//         $(".footer-strip .footer-mainlink h3").removeClass('toggle');
//         $(".footer-strip .footer-mainlink h3").removeClass('active');
//          $(".footer-strip.mobile_togglemenu").remove()
       
//     }
// }
// $(document).ready(mobileToggleColumn);